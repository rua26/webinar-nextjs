import styled from "styled-components";

export const TextTitle = {
    fontSize: "var(--medium-text-3)",
    fontWeight: 600,
    display: "flex",
    alignItems: "center",
    textAlign: "justify"
}

export const WhyChooseComponent = styled.section`
    margin-bottom: 15rem;
    height: auto;
    h1 {
        margin-bottom: 6.25rem;
        text-align: center;
    }
    @media (max-width: 720px) {
        margin-bottom: 8rem;
         h1 {
            margin-bottom: 2rem;
            text-align: start;
        }
    };
`