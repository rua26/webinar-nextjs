import styled from "styled-components";

export const Wrapper = styled.footer`
  border-top: 2px solid gray;
  padding: 2rem;
  margin-top: 5rem;
`;

export const FooterComponent = styled.div`   
    // scroll-snap-align: end;
    span {
        font-size: var(--small-text);
    }

    img {
      width: 40px;
      height: auto;
    }
    @media (max-width: 429px) {
      padding: 2rem 0.5rem ;
      span {
        // font-size: var(--xs);
        white-space: nowrap;
      }
    };
    
`;

export const SocialMedia = styled.div`
    width: 40%;
    display: flex;
    justify-content: space-between;
    margin-top: 1rem;
    @media (max-width: 429px) {
      width: 100%;
      padding: 0 2rem;
      margin-bottom: 1rem;
    };
`

export const Col = styled.div`
  display: flex;
  flex-direction: column;
  width: fit-content;
`;

export const LogoCol = styled(Col)`
  align-items: flex-start;
  width: 100%;

  p {
    text-align: start;
    white-space: nowrap;
  }

  img {
    width: 130px;
    margin-bottom: 20px;
  }

  @media (max-width: 429px) {
    align-items: center;
   
    p {
      text-align: center;
      font-size: 16px; 
    }
  };
`;

export const ContactPart = styled.div`
  display: flex;
  justify-content: center;
  align-items: start;
  flex-direction: column;
  margin-top: 0.5rem;
  span {
    white-space: nowrap;
  }

  .contact {
    flex-direction: column;
  }

  @media (max-width: 429px) {
    align-items: center;
    width: 100%;
    .contact {
      flex-direction: row;
      justify-content: space-around;
      width: 100%;
      margin-bottom: 1rem;
    }
  };
`;