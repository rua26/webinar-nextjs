import styled from "styled-components";

import { SlideIn } from "./animation";

export const GreadMindComponent = styled.section`
    margin-bottom: 10rem;
    h1 {
        padding-bottom: 4rem;
    }
`;

export const Rectangle = styled.div`
    width: 1.125rem;
    height: 0.5rem;
    border-radius: 1rem;
    background-color: var(--light);
    margin: 0 3px;
`;

export const Lines = styled.div`
    padding-top: 2rem;
    display: flex;
    justify-content: center;
    @media (max-width: 429px) {
        display: none;
    };
`

export const Flag = styled.div`
    width: 42px;
    height: 42px;
    position: absolute;
    top: 0.1rem;
    right: 2rem;
    // border: 5px solid var(--light);
    // border-radius: 45px;
`;

export const ImageDescription = styled.div`
    position: absolute;
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    bottom: 1rem;
    margin: 0 1.5rem;
    z-index: 1;
    h5 {
        margin: 0;
        font-size: var(--xs);
        font-weight: 700;
        text-align: start;
    }
    span {
        font-size: var(--xs);
        font-weight: 500;
    }

    @media (max-width: 729px) {
        h5, span {
            font-size: var(--normal-text);
        }
    };
`;

export const Img = styled.div`
    aspect-ratio: 1;
    width: 400px;
    height: 350px;
    border-radius: 10px;
    position: relative;
    background: url(${({ src }) => (src ? `${src}` : null)});
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    background-color: var(--light);
    @media (max-width: 769px) {
        height: 60vh;
    };
`;

export const Animation = styled.div`
    animation: ${SlideIn};
    animation-duration: ${({ duration, trigger }) => (duration ? `${duration}s` : "0s")};
    animation-iteration-count: 1;
    animation-timing-function: ease-in;
    // animation-delay: 3s;
    @media (max-width: 769px) {
        animation-duration: ${({ duration, trigger }) => (duration ? `${duration/44}s` : "0s")};
    };
`;
