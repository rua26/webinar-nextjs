import styled from "styled-components";

import { FadeIn, ShowUpAnimation } from "./animation";

export const HeaderComponent = styled.header`
    height: 100vh;
    background-color: black;
`;

export const Wrapper = styled.div`
  height: 100%;
  padding: 1rem 0;
  display: flex;
  justify-content: center;
  align-items: center;

  > a > img {
    width: 120px;
    margin: 20px 0;
  }

  @media (max-width: 1024px) {
    font-size: 12px;
  }

  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
   user-select: none;
`;

const centerElement = `
    position: absolute;
    top: 0;
    left: 0;
    display: flex;
    align-items: center;
    justify-content: center;
    height: 100%;
    width: 100%;
`;

export const HeroComponent= styled.div`
    position: relative;
    video {
        width: 99%;
        object-fit: cover;
        height: 45vh;
        @media (max-width: 429px) {
            height: 35vh;
        };
    };

    .hero-text {
        ${centerElement}
        h1 {
            font-family: 'iCiel Gotham Ultra', sans-serif;
            font-size: 4rem;
            font-weight: 500;
            whitespace: nowrap;
            animation: ${ShowUpAnimation};
            animation-duration: 3s;
            animation-iteration-count: 1;
            animation-delay: 1s;
            @media (max-width: 769px) {
                font-size: 1.65rem;
                line-height: 3.75rem;
                letter-spacing: normal;
            }
        }
    }

    .hero-text-overlay {
        ${centerElement}
        mix-blend-mode: multiply;
        background: black;
        color: var(--primary-text);
        h1 {
            font-size: 9.375rem;
            font-weight: 900;
            letter-spacing: 0.625rem;
            line-height: 8.75rem;
            font-family: 'iCiel Gotham Ultra', sans-serif;
            animation: ${ShowUpAnimation};
            animation-duration: 3s;
            animation-iteration-count: 1;
            @media (max-width: 769px) {
                font-size: 8rem;
                line-height: 7rem;
                letter-spacing: normal;
            };
            @media (max-width: 550px) {
                font-size: 5rem;
                line-height: 4.5rem;
                letter-spacing: normal;
            };
        };
    };

    .hero-time {
        position: absolute; 
        left: 0; 
        right: 0; 
        bottom: 20px;
        margin-left: auto; 
        margin-right: auto;
        h1 {    
            font-weight: 900;
            font-family: 'iCiel Gotham bold', sans-serif;
        }

        @media (max-width: 769px) {
            h1 {
                font-size: 1.5rem;
            }
        };
    }

    .ellipes {
        z-index: -1;
        animation: ${ShowUpAnimation};
        animation-duration: 5s;
        animation-iteration-count: 1;
    }
`;

export const CountDown = styled.div`
    width: 60%;
    margin: 1rem auto 1rem auto;
    h1 {
        font-size: 5rem;
    };

    p {
        font-size: 1rem;
    };
    @media (max-width: 769px) {
        width: 100%;
        margin: 1rem auto 1rem auto;
        h1 {
            font-size: 3rem;
        }
        p {
            font-size: 1rem;
        }
    };
`;

export const Description = styled.div`
    margin-bottom: 1rem;
    // height: 20vh;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    align-items: center;
    z-index: 1000;
    animation: ${FadeIn};
    animation-duration: 3s;
    animation-iteration-count: 1;
    white-space: nowrap;
    h1 {
        margin-bottom: 1rem;
        font-size: var(--medium-text-2);
    }
    h2 {
        font-size: var(--medium-text-3);
    }
    font-family: 'iCiel Gotham Bold', sans-serif;
    @media (max-width: 900px) {
        h2 {
            font-size: var(--normal-text);
        }
    };
    @media (max-width: 550px) {
        h1 {
            font-size: var(--medium-text-3);
        }
        h2 {
            font-size: 11px;
        }
    };
`;

export const HeroFooter = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
`