import { keyframes } from 'styled-components'

export const SlideIn = keyframes`
    0% {transform: translateX(1000px); opacity: 0;}
    100% {transform: translateX(0); opacity: 1;}
`;

export const SlideOut = keyframes`
    0% {transform: translateX(0); opacity: 1;}
    100% {transform: translateX(-200px); opacity: 0;}
`;

export const FadeIn = keyframes`
    0% {transform: translateY(150px); opacity: 0;}
    100% {transform: translateY(0); opacity: 1;}
`;

export const ShowUpAnimation = keyframes`
    0% {opacity: 0;}
    100% {opacity: 1;}
`;

export const Spin3D = keyframes`
    0% { transform: rotateY(0) }
    100% { transform: rotateY(360deg)}
`;

export const VibrationAnimation = keyframes`
  0% { transform: translate(30px, 30px) rotate(0deg); }
  10% { transform: translate(-20px, -20px) rotate(-5deg); }
  20% { transform: translate(-25px, 14px) rotate(5deg); }
  30% { transform: translate(25px, 13px) rotate(0deg); }
  40% { transform: translate(20px, -20px) rotate(5deg); }
  50% { transform: translate(-16px, 12px) rotate(-5deg); }
  60% { transform: translate(-15px, 8px) rotate(0deg); }
  70% { transform: translate(7px, 15px) rotate(-5deg); }
  80% { transform: translate(-8px, -8px) rotate(5deg); }
  90% { transform: translate(6px, 7px) rotate(0deg); }
  100% { transform: translate(10px, -10px) rotate(-5deg); }
`;