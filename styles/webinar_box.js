import styled from "styled-components";

export const Box = styled.div`
  background: var(--grey);
  border-radius: 1rem;
  display: flex;
  padding: 20px 25px;
  justify-content: space-between;
  align-items: center;
  flex-direction: column;
  height: auto;
  margin-top: 20px;
  position: relative;
  @media (min-width: 768px) {
    min-height: 13rem;
    flex-direction: row;
    padding: 20px 34px;
  } ;
  @media (min-width: 1024px) {
    min-height: 11rem;
    padding: 5px 20px;
  };
  @media (min-width: 1200px) {
    min-height: 13rem;
    padding: 10px 15px;
  };
`;

export const BoxHeader = styled.div`
  display: flex;
  align-content: flex-start;
  align-items: center;
  width: 100%;
  @media (min-width: 768px) {
    justify-content: space-between;
    flex-direction: row-reverse;
    align-items: flex-start;
  } ;
`;

export const Button = styled.div`
  padding: 2px 10px;
  border: 1.65px solid var(--border-button-color);
  border-radius: 8.25px;
  font-size: var(--xs);
  text-align: center;
  margin-right: 12px;
  position: absolute;
  min-width: 4.8rem;
  top: 1rem;
  left: 1.2rem;
  @media (min-width: 768px) {
    top: 1.2rem;
    right: 0.5rem;
    left: auto;
  } ;
  @media (min-width: 1024px) {
    font-size: 12px;
    min-width: 4rem;
    padding: 2px 8px;
  } ;
  @media (min-width: 1200px) {
    font-size: var(--xs);
    min-width: 5rem;
  } ;
`;

export const TimeLine = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-around;
  flex-direction: row;
  margin-left: 4rem;
  @media (min-width: 768px) {
    flex-direction: column;
    position: relative;
    margin-left: 4rem;
  };
  @media (min-width: 1024px) {
    margin-left: 3rem;
  };
  @media (min-width: 1200px) {
    margin-left: 4.5rem;
  };
`;

export const Time = styled.span`
  font-size: var(--xs);
  color: #6e6e6e;
  text-align: center;
  white-space: nowrap;
  font-weight: 600;
  margin-right: 0.5rem;
  @media (min-width: 768px) {
    font-size: var(--small-text);
  } ;
  @media (min-width: 1024px) {
    font-size: 12px;
  } ;
  @media (min-width: 1200px) {
    font-size: var(--small-text);
  } ;
`;

export const TimeStart = styled(Time)`
  @media (min-width: 768px) {
    position: absolute;
    top: -3px;
    right: 15px;
  } ;
`;

export const TimeEnd = styled(Time)`
  @media (min-width: 768px) {
    position: absolute;
    bottom: -10px;
    right: 15px;
  } ;
`;

export const Seperate = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 0 7px;
  @media (min-width: 768px) {
    flex-direction: column;
  } ;
`;

export const Circle = styled.div`
  width: 0.4rem;
  height: 0.4rem;
  background: #272727;
  border-radius: 0.4rem;
`;

export const Line = styled.div`
  width: 3.2rem;
  height: 3px;
  background: #272727;
  border: 2.35px solid #2727272;
  @media (min-width: 768px) {
    width: 0.2rem;
    height: 10rem;
  } ;
  @media (min-width: 1024px) {
    height: 7rem;
  };
  @media (min-width: 1200px) {
    height: 9rem;
  } ;
`;
export const BoxContent = styled.div`
  display: flex;
  flex-direction: column;
  align-content: flex-start;
  width: 100%;
  @media (min-width: 768px) {
    margin-left: 1rem;
  } ;
  @media (min-width: 1024px) {
    margin-left: 0.5rem;
    margin-bottom: 0.5rem;
  } ;
  @media (min-width: 1200px) {
    margin-left: 1rem;
    margin-bottom: 0rem;
    min-height: 10rem;
  } ;
`;

export const Speaker = styled.div`
  display: flex;
  justify-content: flex-start;
  margin: 2rem 0rem;
  align-items: center;
  @media (min-width: 768px) {
    margin-top: 0.5rem;
    margin-bottom: 3rem;
  } ;
  @media (min-width: 1024px) {
    margin-top: 0.5rem;
    margin-bottom: 1.5rem;
  } ;
  @media (min-width: 1200px) {
    margin-bottom: 3rem;
  } ;
`;

export const Avatar = styled.div`
  width: 3.75rem;
  height: 3.75rem;
  border-radius: 3.75rem;
  background: url(${({ src }) => (src ? `${src}` : null)});
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
//   @media (min-width: 768px) {
//     width: 4rem;
//     height: 4rem;
//   };
  @media (min-width: 1024px) {
    width: 3rem;
    height: 3rem;
  } ;
  @media (min-width: 1200px) {
    width: 4rem;
    height: 4rem;
  } ;
`;

export const Profile = styled.div`
  display: flex;
  flex-direction: column;
  text-align: start;
  margin-left: 1rem;
`;

export const Name = styled.h4`
  font-weight: 600;
  font-size: var(--normal-text);
  margin-bottom: 0;
  @media (min-width: 1024px) {
    font-size: var(--xs);
  };
  @media (min-width: 1200px) {
    font-size: var(--normal-text);
  };
`;

export const Title = styled.div`
  font-size: var(--xs);
  line-height: 16px;
  @media (min-width: 768px) {
    max-width: 20rem;
  };
  @media (min-width: 1024px) {
    font-size: 10px;
    max-width: 12rem;
  };
  @media (min-width: 1200px) {
    font-size: var(--xs);
    max-width: 17rem;
  };
`;

export const Topic = styled.h1`
  font-size: var(--medium-text);
  font-weight: 600;
  text-align: start;
  margin: 1rem 0;
  @media (min-width: 1024px) {
    margin: 0 0 1rem 0;
    font-size: 13px;
  } ;
  @media (min-width: 1200px) {
    margin: 0;
    font-size: var(--medium-text);
  };
`;

export const PanelContent = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  @media (min-width: 768px) {
    margin-left: 1rem;
    h1 {
        width: 100%;
        margin-bottom: 1rem;
    }
  } ;
  @media (min-width: 1024px) {
    margin-left: 0.5rem;
    h1 {
        max-width: 17rem;
    }
  } ;
  @media (min-width: 1200px) {
    margin-left: 1rem;
    h1 {
        margin-bottom: 1rem;
        max-width: 22rem;
    }
  } ;
`;

export const Speakers = styled.div`
  display: flex;
  flex-wrap: wrap;
  // justify-content: space-between;
  gap: 5%;
  @media (min-width: 768px) {
    gap: 5%;
  } ;
`;

export const TextGradient = styled.span`
  background: linear-gradient(90.82deg, #ff00cc 1.13%, #4141c1 83.76%);
  -webkit-background-clip: text;
  -webkit-text-fill-color: transparent;
  background-clip: text;
  font-weight: 700;
  text-fill-color: transparent;
  margin-top: 0.5rem;
  white-space: nowrap;
  font-size: var(--xs);
  @media (min-width: 1024px) {
    font-size: 10px;
  };
  @media (min-width: 1200px) {
    font-size: 13px;
  };
`;

export const SpeakerBox = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 25%;
  @media (min-width: 768px) {
    width: 15%;
  } ;
`;

