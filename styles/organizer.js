import styled from 'styled-components';

export const OrganizerSection = styled.section`
    display: flex;
    align-items: center;
    justify-content: center;
    overflow: hidden;
    margin-bottom: 15rem;
    video {
        height: 62vh;
        width: 29vw;
        transform: scale(1.5);
        z-index: -1;
        object-fit: cover;
    }

    @media (max-width: 720px) {
        // height: 58rem;
        margin-bottom: 4rem;
        video {
            height: 360px;
            width: 100%;
            transform: scale(1);
        }
    };
`;