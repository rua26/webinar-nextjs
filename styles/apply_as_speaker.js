import styled from "styled-components";


export const ApplyAsSpeakerComponent = styled.div`
    height: auto;
    padding-bottom: 8rem;
`;

export const DescriptionPart = styled.div`
  height: 350px;
  width: 100%;
  position: relative;
  z-index: -1;
  text-align: justify;
  .text-part {
    width: calc(100% + 13rem);
    background: rgba(255, 255, 255, 0.01);
    z-index: 1;
    box-shadow: 0px 10px 20px 5px rgba(0, 0, 0, 0.3);
    backdrop-filter: blur(40px);
    /* Note: backdrop-filter has minimal browser support */
    padding: 4.5rem 12rem 4.5rem 3rem;
    border-radius: 2rem;
    position: relative;
  }

  .text-part::before {
    content: "";
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    border-radius: 2rem; 
    border: 2px solid transparent;
    background: linear-gradient(75deg,white,black) border-box;
    -webkit-mask:
      linear-gradient(#fff 0 0) padding-box, 
      linear-gradient(#fff 0 0);
    -webkit-mask-composite: destination-out;
    mask-composite: exclude;
  }

  p {
    font-size: var(--xs);
    margin-top: 1.5rem;
  }

  @media (max-width: 1024px) {
    .text-part {
      width: 100%;
      padding: 1rem;
    }
  };
`;

export const EmbedForm = styled.iframe`
  height: 1100px;
  width: 100%;
  color: white;
  border-radius: 25px;
`;
