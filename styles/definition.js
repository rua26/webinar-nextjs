import styled from 'styled-components';

export const DefinitionSection = styled.section`
    margin: 10rem 0;
    display: flex;
    align-items: center;
    justify-content: center;
    video {
        height: auto;
        width: 100%;
        transform: scale(1.5);
        z-index: -1;
        object-fit: cover;
    }

    @media (max-width: 700px) {
        margin-top: 0rem;
        margin-bottom: 5rem;
        video {
            height: 40vh;
            width: 100%;
            transform: scale(1);
        }
    };
`;