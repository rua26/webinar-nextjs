import styled from "styled-components";

export const WebinarComponent = styled.section`
    overflow: hidden;
    margin-bottom: 10rem;

    @media (max-width: 429px) {
        .webinar-title {
            flex-direction: column;
        }
    };
`;

export const Pagination = styled.div`
    display: flex;
    align-items: flex-end;
    justify-content: space-between;
    padding-bottom: 2rem;
    width: 4rem;

    span {
        cursor: pointer;
    }

    span:hover {
        color: var(--secondary-bg);
    }


    @media (max-width: 429px) {
        width: 100%;
    };
`;

export const Buttons = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    margin: 1rem 0;
    gap: 5%;
    @media (min-width: 725px) {
        margin-top: 90px;
        gap: 2%;
    };
`;
