import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import Container from "react-bootstrap/Container";
import axios from "axios";
import {
    WhyChooseComponent,
    TextTitle,
} from "../styles/why_choose";

export async function getStaticProps() {
    const res = await axios.get('https://api-content.wow-ai.com/wowai/getdetail/35558');
    const additionData = await res.json();

    return {
        props: {
            additionData,
        }
    }
}

const WhyChoose = ({ additionData }) => {
    return (
        <WhyChooseComponent
            id="why-choose"
            className="section-scroll"
        >
            <h1 className="title-content">Why you should join <br></br>
                <span className="text-gradient"> Worldwide AI Webinar </span>
                2022?
            </h1>
            <Container>
                <Row className="text-left border-bottom border-light pb-4 pt-4">
                    <Col lg={4} xs={12} className="text-start" style={TextTitle}>
                        Join the Global AI Community
                    </Col>
                    <Col lg={8} xs={12} className="text-start">
                        Go on a unique journey through a unique 3D immersive virtual environment with over 10,000 AI professionals.
                    </Col>
                </Row>
                <Row className="text-left border-bottom border-light pb-4 pt-4">
                    <Col lg={4} xs={12} className="text-start" style={TextTitle}>
                        Deepen your understanding of AI
                    </Col>
                    <Col lg={8} xs={12} className="text-start">
                        Listen to the world&apos;s foremost experts from tech giants introduce the new age’s technological marvels.
                    </Col>
                </Row>
                <Row className="text-left border-bottom border-light pb-4 pt-4">
                    <Col lg={4} xs={12} className="text-start" style={TextTitle}>
                        Tackling your business challenges
                        with AI experts
                    </Col>
                    <Col lg={8} xs={12} className="text-start">
                        Learn from some of the most influential decision makers from 5 continents , who have already seen the potential of AI powered business, and how to implement best practices for your business to solve your toughest problems.

                    </Col>
                </Row>
                {additionData &&
                    (
                        <Row className="text-left border-bottom border-light pb-4 pt-4">
                            <Col lg={4} xs={12} className="text-start" style={TextTitle}>
                                {additionData.Title}
                            </Col>
                            <Col lg={8} xs={12} className="text-start">
                                {additionData.Description}
                            </Col>
                        </Row>
                    )
                }
            </Container>
        </WhyChooseComponent>
    );
};

export default WhyChoose;
