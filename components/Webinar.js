import {useState} from "react";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import {
    WebinarComponent,
    Buttons,
} from '../styles/webinar';

import WebinarBox from "./WebinarBox";

const Webinar = ({ webinars }) => {
    const [date, setDate] = useState(0);
    return (
        <WebinarComponent
            id = "webinar"
            className = "section-scroll"
        >
            <div className="webinar-title d-flex justify-content-between">
                <h1 className="title-content">
                    Worldwide AI Webinar Agenda
                </h1>
            </div>
            <Row>
                <Buttons>
                    <div 
                        className={date === 0 ? "btn-gradient-small" : "btn-outline"}
                        onClick={() => setDate(0)}
                    >
                        <span>Sept 29</span>
                    </div>
                    <div 
                        className={date === 1 ? "btn-gradient-small" : "btn-outline"}
                        onClick={() => setDate(1)}
                    >
                        <span>Sept 30</span>
                    </div>
                </Buttons>
                {webinars[date]["values"]?.map(({ id, type, values, startAt, endAt}) => (
                    <Col lg={6} md={12} xs={12} key={id}>
                        <WebinarBox 
                            speaker={values} 
                            type={type} 
                            startAt={startAt}
                            endAt={endAt}
                        />
                    </Col>
                ))}
            </Row>
        </WebinarComponent>
    );
};

export default Webinar;