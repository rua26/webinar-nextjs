import { useEffect, useRef } from "react";

const Video = ({src}) => {
    const vidRef = useRef(null);

    useEffect(() => {
        if(vidRef) vidRef.current.play();
    }, []);

    return (
        <video ref={vidRef} autoPlay loop muted 
            playsInline 
            data-bgvideo="" 
            data-bgvideo-fade-in="500" 
            data-bgvideo-pause-after="120" 
            data-bgvideo-show-pause-play="true" 
            data-bgvideo-pause-play-x-pos="right" 
            data-bgvideo-pause-play-y-pos="top">
            <source src={src} type="video/mp4"/>
            Your browser does not support the video tag.
        </video>
    )
};

export default Video;