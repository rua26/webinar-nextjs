import {
    Box,
    BoxHeader,
    Button,
    TimeLine,
    Time, 
    TimeStart, 
    TimeEnd,
    Seperate,
    Circle,
    Line,
    BoxContent,
    Speaker,
    Avatar,
    Profile,
    Name,
    Title,
    Topic,
    PanelContent,
    Speakers,
    TextGradient,
    SpeakerBox,
} from "../styles/webinar_box";

export const PanelBox = ({ data }) => {
    return (
      <PanelContent>
        <Topic>How to build Responsible Applied AI Solutions at Scale</Topic>
        <Speakers>
          {data.map(({ id, avatar, company }) => (
            <SpeakerBox key={id}>
              <Avatar src="images/ali.jpeg" />
              <TextGradient>{company}</TextGradient>
            </SpeakerBox>
          ))}
        </Speakers>
      </PanelContent>
    );
  };

  export const Schedule = ({ startAt, endAt }) => {
    return (
      <TimeLine>
        <TimeStart>{startAt}</TimeStart>
        <Seperate>
          <Circle />
          <Line />
          <Circle />
        </Seperate>
        <TimeEnd>{endAt}</TimeEnd>
      </TimeLine>
    );
  };
  
  export const KeynoteBox = ({ data }) => {
    const { name, title, topic } = data;
  
    const extractWordGradient = (string) => {
      const re = /@\w*/g;
      const found = typeof string === 'string' ? string.match(re) :  "";
      if (found) {
          for(let i = 0; i < found.length; i++) {
              string = string.replace(found[i], `<span class="text-gradient-2">${found[i]}</span>`)
          }
      }
  
      return string;
    };
  
    return (
      <BoxContent>
        <Speaker>
          <div>
            <Avatar src="images/ali.jpeg" />
          </div>
          <Profile>
              <Name>Speaker: {name}</Name>
              <Title dangerouslySetInnerHTML={{__html: extractWordGradient(title)}} />
          </Profile>
        </Speaker>
        <Topic>{topic}</Topic>
      </BoxContent>
    );
  };
  
  const WebinarBox = ({ speaker, type, startAt, endAt }) => {
    const capitalizeFirstLetter = (string) => {
      return string.charAt(0).toUpperCase() + string.slice(1);
    };
  
    return (
      <Box>
        <Button>{capitalizeFirstLetter(type)}</Button>
        <Schedule startAt={startAt} endAt={endAt} />
        {type === "panel" ? (
          <PanelBox data={speaker} />
        ) : (
          <KeynoteBox data={speaker} />
        )}
      </Box>
    );
  };
  
  export default WebinarBox;
  