import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import Link from "next/link";
import { Ellipse } from "./Ellipse";
import {
  ApplyAsSpeakerComponent,
  DescriptionPart,
  EmbedForm,
} from "../styles/apply_as_speaker";

const ApplyAsSpeaker = () => {
  return (
    <ApplyAsSpeakerComponent
      id="apply-as-speaker" 
    >
        <Row>
          <Col lg={6} xs={12} className="d-flex align-items-center">
              <DescriptionPart className="d-flex flex-column justify-content-center">
                  <div className="text-part">
                    <h1 className="text-start title-content">Apply as Link Speaker</h1>
                    <p className="text-justify">Worldwide AI Webinar aims to do an annual conference and pocket round table meetings 
                        in different countries across the world. Share your expertise and best practices with your colleagues. 
                        Be part of the AI great minds community!
                    </p>
                  </div>
                  <div className="ellipses">
                    <Ellipse width="70px" height="70px" degree="98.4deg" top="85%" left="70%"/>
                    <Ellipse width="120px" height="120px" degree="-21deg" top="-15%" left="10%"/>
                  </div>
              </DescriptionPart>
          </Col>
          <Col lg={6} xs={12} style={{ zIndex: 1 }}>
            <EmbedForm 
              src="https://webforms.pipedrive.com/f/6WfGTu1ydVop966sqIESJzGXPA3g6VOlZR0jNBcyZxhUG8jZBtEzfn8MnLlY28Bpir" 
            />
          </Col>
        </Row>
    </ApplyAsSpeakerComponent>
  );
};

export default ApplyAsSpeaker;
