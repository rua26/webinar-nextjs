import { useContext } from 'react';
import Link from 'next/link';
import { OrganizerSection } from '../styles/organizer';

import { Col, Row } from 'react-bootstrap';
import { UserContext } from "../store";

import Video from "./Video";

export const organizerVideo = "https://storage-b.wow-ai.com/video/organizer.mp4";
export const organizerVideoMobile = "https://storage-b.wow-ai.com/video/organizer_mobile.mp4";

const Organizer = () => {
    const width = useContext(UserContext);

    return (
        <OrganizerSection
            id="definition"
            className="section-scroll"
        >
            <Row>
                <Col xs={12} lg={6} className="d-flex flex-column justify-content-center">
                    <h5 className="title-content left">Wow AI <br></br>The Organizer</h5>
                    <div style={{ textAlign: "justify" }}>
                        <div className="title-description">
                            Wow AI is a global provider of high-quality AI training data. 
                            We are experienced professionals with a track record of success in numerous types of data collection. 
                            With a crowd of over 100,000 project contributors all over the world, we can provide massive, scalable, high-quality data across data types.
                            <p></p>
                            <Link href="https://wow-ai.com/" target="_blank" rel="noreferrer">
                                <b>Visit our website</b>
                            </Link>
                        </div>
                    </div>
                </Col>

                <Col xs={12} lg={6} className="d-flex justify-content-center align-items-center">
                    <Video src={width <= 429 ? organizerVideoMobile : organizerVideo} />
                </Col>
            </Row>
        </OrganizerSection>
    )
}

export default Organizer;