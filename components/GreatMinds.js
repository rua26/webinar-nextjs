import React, { useState, useEffect, useContext, useRef } from "react";
import { Col, Row, Container } from "react-bootstrap";
import { 
    GreadMindComponent,
    Flag,
    ImageDescription,
    Lines,
    Rectangle,
    Img,
    Animation,  
} from "../styles/great_minds";
import { Pagination } from "../styles/webinar";
import minds from "../config/constants";
import ReactCountryFlag from "react-country-flag";
import { FiChevronLeft, FiChevronRight } from "react-icons/fi";
import { UserContext } from "../store";

const GreateMinds = () => {
    const numMinds = minds.length;
    const initialNumberPage = Math.ceil(numMinds/4);
    const [range, setRange] = useState(4);
    const [startIndex, setStartIndex] = useState(0);
    const [endIndex, setEndIndex] = useState(0);
    const [numberPage, setNumberPage] = useState(initialNumberPage);
    const [currentPage, setCurrentPage] = useState(1);
    const [delay, setDelay] = useState(10000);

    const timeoutRef = useRef(null);
    const width = useContext(UserContext);

    const determineRange = () => {
        let r;
        if (width <= 769) {
            r = 1; 
        } else if (width > 769 && width < 1024) {
            r = 3;
        } else {
            r = 4;
        };
        return r;
    };

    function resetTimeout() {
        if (timeoutRef.current) {
          clearTimeout(timeoutRef.current);
        };
    };

    useEffect(() => {
        const value = width < 769 ? 3000 : 10000;
        setDelay(value);
    }, []);

    useEffect(() => {
        resetTimeout();
        let r = determineRange();
        timeoutRef.current = setTimeout(
        () => {
            const start = startIndex + r < numMinds ? (startIndex + r) : 0;
            setStartIndex(start);
            setEndIndex(start + r);
            setCurrentPage(prev => (prev < numberPage) ? prev + 1 : 1);
        },
        delay);

        return () => {
            resetTimeout();
        };
    }, [currentPage]);

    useEffect(() => {
        let newRange, end;
        newRange = determineRange();
        end = startIndex + newRange;
        setRange(newRange);
        setStartIndex(0);
        setEndIndex(end);
        setNumberPage(Math.ceil(numMinds/newRange));
    }, [width]);

    const previousPage = () => {
        if (startIndex === 0) return;
        const start = startIndex - range;
        const end = start + range >= numMinds ? numMinds : (start + range);
        setStartIndex(start);
        setEndIndex(end);
        setCurrentPage(prev => prev - 1);
    };

    const nextPage = () => {
        if (endIndex >= numMinds) return;
        const start = startIndex + range;
        const end = start + range >= numMinds ? numMinds : (start + range);
        setStartIndex(start);
        setEndIndex(end);
        setCurrentPage(prev => prev + 1);
    };

    return (
        <GreadMindComponent
            id="greate_minds"
            className="section-scroll"
        >
            <div className="minds-title d-flex flex-column justify-content-end">
                <h1 className="title-content text-center">
                    <span className="text-gradient">Great Minds</span> Of Worldwide <br></br> AI Webinar 2022
                </h1>
                <Pagination className="d-flex justify-content-between w-100">
                    <span onClick={previousPage}>
                        <FiChevronLeft size="2rem"></FiChevronLeft>
                    </span>
                    <span onClick={nextPage}>
                        <FiChevronRight size="2rem"></FiChevronRight>
                    </span>
                </Pagination> 
            </div>
            <Container>
                <Row>
                    {minds.slice(startIndex, endIndex).map(({id, src, name, country, position, company}, index) => (
                        
                        <Col 
                            xs={12}
                            md={4}
                            lg={3}
                            key={id}
                            className = "position-relative"
                        >
                            <Animation duration={index + 1} trigger={true}>
                                <div className= "img-gradient">
                                    <Img 
                                        alt={name}
                                        className="w-100"
                                        src="images/ali.jpg"
                                        style={{borderRadius: "10px"}}
                                    />
                                </div>
                            
                                <Flag>
                                    <ReactCountryFlag 
                                        countryCode={country}
                                        style={{
                                            fontSize: '2em',
                                            lineHeight: '2em',
                                        }}
                                    />
                                </Flag>
                                <ImageDescription>
                                    <h5>{name}</h5>
                                    <span className="text-start">{position}</span>
                                    <h5>{company}</h5>
                                </ImageDescription>
                            </Animation>
                        </Col>
                    ))}
                </Row>
                <Lines>
                    {Array.from(Array(numberPage)).map((page, index) => (
                        <Rectangle 
                            key={index} 
                            style={{ 
                                width: (index + 1) === currentPage ? "2rem" : "1.25rem",
                                backgroundColor: (index + 1) === currentPage ? "var(--secondary-bg)" : "var(--light)"
                        }}>
                        </Rectangle>
                    ))}
                </Lines>
            </Container>
        </GreadMindComponent>
    )
};

export default GreateMinds;
