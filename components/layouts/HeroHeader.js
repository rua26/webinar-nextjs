import React, { useState, useEffect, useContext } from "react";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { Ellipse } from "../Ellipse";
import {
    CountDown,
    HeroComponent,
    Description,
    HeroFooter,
} from "../../styles/header";
import { UserContext } from "../../store";
import { ButtonGradient } from "../ButtonGradient";
import Video from "../Video";

export const heroHeaderVideoMobile =  "https://storage-b.wow-ai.com/video/hero_header_mobile.mp4";
export const heroHeaderVideo =  "https://storage-b.wow-ai.com/video/hero_header.mp4";

const HeroHeader = ({openModal}) => {
    const width = useContext(UserContext);

    const initialTime = {
        "d": 10,
        "h": 10,
        "m": 10,
        "s": 10, 
    };
    const countDownDate = new Date("September 29, 2022 09:00:00").getTime();
    const [time, setTime] = useState(initialTime);
    const [showCountDown, setShowCountDown] = useState(false); 

    useEffect(() => {
        const countDown = setInterval(() => {
            const expriedTime = 24*30*60*60*1000;
            const now = new Date().getTime();

            const distance = countDownDate - now;

            setShowCountDown(distance <= expriedTime);

            let days = Math.floor(distance / (1000 * 60 * 60 * 24));
            let hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            let seconds = Math.floor((distance % (1000 * 60)) / 1000);

            const objectTime = {
                "d": days,
                "h": hours,
                "m": minutes,
                "s": seconds, 
            };

            setTime(objectTime);

            if (distance < 0) {
                clearInterval(countDown);
            };
        }, 1000);
    }, [countDownDate]);
    
    return (
        <div className="section-scroll mx-auto">
            <HeroComponent>
                   <Video src={width <= 429 ? heroHeaderVideoMobile : heroHeaderVideo }/>
                    <div className="hero-text-overlay">
                        <h1>WORLD<br></br>WIDE</h1>
                    </div>
                    <div className="hero-text">
                        <h1>AI WEBINAR 2022</h1>
                    </div>
                    <div className="d-none d-md-block ellipes">
                            <Ellipse width="70px" height="70px" degree="-151deg" top="20%" left="15%"/>
                            <Ellipse width="120px" height="120px" degree="-21deg" top="70%" left="80%"/>
                            <Ellipse width="60px" height="60px" degree="-21deg" top="80%" left="10%" filter="4px"/>
                            <Ellipse width="40px" height="40px" degree="-21deg" top="25%" left="85%" filter="6px"/>
                       
                    </div>
            </HeroComponent>
            {!showCountDown ? (
                <Description>
                    <h1>29th - 30th Sept</h1>
                    <h2 className="">An Annual Global Meeting Of AI Experts To Share Best Practices</h2>
                    <h2>With a special panel discussion on AGI</h2>
                </Description>
            ) : (
                <CountDown>
                    <Row>
                        <Col>
                            <h1>{time.d}</h1>
                            <p>Days</p>
                        </Col>
                        <Col>
                            <h1>{time.h}</h1>
                            <p>Hours</p>
                        </Col>
                        <Col>
                            <h1>{time.m}</h1>
                            <p>Minutes</p>
                        </Col>
                        <Col>
                            <h1>{time.s}</h1>
                            <p>Seconds</p>
                        </Col>
                    </Row>
                </CountDown>
            )}

            <HeroFooter>
                <ButtonGradient className="btn-gradient" handleClick={openModal} />
                <h3 className="">Limited Slots Available</h3>
            </HeroFooter>
        </div>
    );
};

export default HeroHeader;