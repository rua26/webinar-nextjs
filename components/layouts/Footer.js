import { 
    LogoCol,
    FooterComponent,
    SocialMedia,
    ContactPart,
    Wrapper,
} from '../../styles/footer.js';
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import Link from 'next/link';
import Image from 'next/image';

const Footer = () => {
    return (
        <Wrapper>
            <FooterComponent className="contain">
            <Row className="d-flex justify-content-between">
                <Col lg={5} md={6} xs={12} className="d-flex align-items-start flex-column">
                    <LogoCol>
                        <Link href="/">
                            <Image src="/images/logo.png" alt="logo" width="120" height="43"/>
                        </Link>
                        <p>
                        A global provider of high-quality AI training data 
                        </p>
                    </LogoCol>
                    <SocialMedia>
                        <Link href="https://facebook.com/WowAI.LLC/" target="_blank" rel="noreferrer" title="Visit our facebook">
                            <Image src="/icons/facebook.svg" alt="Facebook icon" width="40" height="40"/>
                        </Link>
                        <Link href="https://instagram.com/wowai_global?igshid=YmMyMTA2M2Y=" target="_blank" rel="noreferrer" title="Visit our instagram">
                            <Image src="/icons/instagram.svg" alt="Instagram icon" width="40" height="40"/>
                        </Link>
                        <Link href="https://www.linkedin.com/company/wow-ai-llc/" target="_blank" rel="noreferrer" title="Visit our linkedin">
                            <Image src="/icons/linkedin.svg" alt="Linedin icon" width="40" height="40"/>
                        </Link>
                        <Link href="https://twitter.com/wowai_llc" target="_blank" rel="noreferrer" title="Visit our twitter">
                            <Image src="/icons/twitter.svg" alt="Twitter icon" width="40" height="40" />
                        </Link>
                    </SocialMedia>
                </Col>

                <Col lg={3} md={6} xs={12} className="d-flex flex-column justify-content-end align-items-start">
                    <ContactPart>
                        <div className="contact d-flex">
                            <div className="d-flex mb-2 justify-content-start align-items-center">
                                <Image src="/icons/mail.svg" alt="Mail icon" width="40" height="40" />
                                <Link style={{ marginLeft: "0.25rem" }} href="mailto: contact@wow-ai.com" title="Send email to Wow AI">contact@wow-ai.com</Link>
                            </div>
                            <div className="d-flex justify-content-start mb-2 align-items-center">
                                <Image src="/icons/world_fill.svg" alt="World fill icon" width="40" height="40" />
                                <Link style={{ marginLeft: "0.25rem" }} href="https://wow-ai.com/" target="_blank" rel="noreferrer" title="Visit our website">Wow-ai.com</Link>
                            </div>
                        </div>
                        <div className="d-flex justify-content-start mb-1 align-items-center">
                            <Image src="/icons/location.svg" alt="Location icon" width="40" height="40" />
                            <span style={{ marginLeft: "0.25rem" }}>34th ST STE 1018, NY, USA</span>
                        </div>
                    </ContactPart>
                </Col>
    
            </Row>
            </FooterComponent>
        </Wrapper>
    )
}

export default Footer;