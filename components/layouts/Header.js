import HeroHeader from "./HeroHeader";
import { Button } from "react-bootstrap";
import Link from "next/link";
import Image from "next/image";
import {
    Wrapper,
    HeaderComponent,
} from "../../styles/header";

const Header = ({openModal}) => {
    return (
       <HeaderComponent>
            <div className="contain">
                <Wrapper className="section-scroll justify-content-between">
                    <Wrapper>
                        <Link href="/">
                            <Image src="/images/logo.png" alt="logo" width="120" height="43"/>
                        </Link>
                    </Wrapper>
                    <Wrapper>
                        <Button 
                            className="btn btn-outline-light secondary-text" 
                            onClick={openModal}
                        >
                            Register Now
                        </Button>
                    </Wrapper>
                </Wrapper>
                <HeroHeader openModal={openModal}/>
            </div>
        </HeaderComponent>
    );
};

export default Header;