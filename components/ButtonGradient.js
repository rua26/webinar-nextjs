import styled from "styled-components";

export const MainButton = styled.button`
    width: 200px;
    height: 47px;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);

    text-align: center;
    padding: 8px 30px;
    border: none;
    font-size: var(--medium-text-3);
    font-weight: 700;
    color: var(--light);
    background: linear-gradient(85.98deg, #FF00CC 2.62%, #333399 73.27%);
    box-shadow: 0px 7px 20px rgba(0, 0, 0, 0.2);
    border-radius: 176.804px;
    white-space: nowrap;
    z-index: 1;
    @media (max-width: 769px) {
        width: 150px;
        padding: 8px 15px;
        span {
            font-size: var(--normal-text);
        }
    };
`;

export const Gradient = styled.div`
    width: 200px;
    height: 47px;
    position: absolute;
    top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);

    background: linear-gradient(85.98deg, rgba(255, 0, 204, 0.8) 2.62%, rgba(51, 51, 153, 0.8) 73.27%);
    filter: blur(25px);
    border-radius: 176.804px;
    @media (max-width: 769px) {
        width: 150px;
    };
`;

export const Button = styled.div`
    position: relative;
    width: 200px;
    height: 47px;
    cursor: pointer;
    margin-bottom: 1rem;
`


export const ButtonGradient = ({ handleClick }) => {
    return (
        <Button>
            <MainButton onClick={handleClick}>
                <span>Register Now</span>
            </MainButton>
            <Gradient />
        </Button>
    )
}