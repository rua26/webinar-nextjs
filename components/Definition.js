import { useContext } from 'react';

import { DefinitionSection } from '../styles/definition';

import Row from 'react-bootstrap/Row';
import Col from "react-bootstrap/Col";

import { UserContext } from "../store";

import Video from "./Video";

export const worldWideVideo = "https://storage-b.wow-ai.com/video/AI_worldwide.mp4";
export const worldWideVideoMobile = "https://storage-b.wow-ai.com/video/AI_worldwide_mobile.mp4";

const Definition = () => {
    const width = useContext(UserContext);

    return (
        <DefinitionSection
            id="definition"
            className="contain"
        >
            <Row>
                <Col xs={12} lg={6} className="d-flex justify-content-center align-items-center">
                    <Video src={width <= 429 ? worldWideVideoMobile : worldWideVideo}></Video>
                </Col>

                <Col xs={12} lg={6} className="d-flex flex-column justify-content-center">
                    <h5 className="title-content left">What&apos;s Worldwide <br></br> AI Webinar?</h5>
                    <div style={{ textAlign: "justify" }}>
                        <div className="title-description">
                            Driven by the goal of of making the future better using the power of AI, 
                            Wow AI is holding an annual global summit of the the brightest minds in AI from 5 continents. 
                            Our attendees will have the opportunity to engage with well known figures from IBM, 
                            Google, JP Morgan Chase, AWS and others and share their experiences using AI in the past, 
                            present, and future. Prepare yourself for mind-blowing discoveries, intelligent conversations, 
                            practical business solutions, and amazing networking opportunities with over <b>10.000 attendees!</b>
                        </div>
                    </div>
                </Col>
            </Row>
        </DefinitionSection>
    )
}

export default Definition;