import React, {useState} from "react";
import Header from 'components/layouts/Header';
import Definition from 'components/Definition';
import GreateMinds from 'components/GreatMinds';
import Webinar from 'components/Webinar';
import WhyChoose from 'components/WhyChoose';
import ApplyAsSpeaker from 'components/ApplyAsSpeaker';
import Organizer from 'components/Organizer';
import Footer from 'components/layouts/Footer';
// import RegisterModal from "components/RegisterModal";

export async function getStaticProps() {
  const res = await fetch("https://62fa9c89ffd7197707f012ff.mockapi.io/api/v1/speakers");
  const webinars = await res.json();

  return {
      props: {
          webinars,
      }
  }
};

function App({webinars}) {
  const [showModal, setShowModal] = useState(false);

  return (
    <div className="App">
      <Header openModal={() => setShowModal(true)}/>
      <div className="contain">
        {/* <RegisterModal 
          show={showModal}
          closeModal={() => setShowModal(false)} 
          openModal={() => setShowModal(true)}
        />  */}
        <Definition />
        <GreateMinds />
        <Webinar webinars={webinars}/>
        <Organizer />
        <WhyChoose />
        <ApplyAsSpeaker />
      </div> 
    <Footer />
    </div>
  );
}

export default App;
