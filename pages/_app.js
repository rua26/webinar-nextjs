import 'bootstrap/dist/css/bootstrap.css';
import '../styles/globals.css';
import { useState, useEffect } from 'react';
import {UserContext} from '../store';
import Head from "next/head";
function MyApp({ Component, pageProps }) {
  const [windowWidth, setWindowWidth] = useState(0);

  const updateDimensions = () => {
    const w = window.innerWidth;
    setWindowWidth(w);
  };

  useEffect(() => {
    updateDimensions();
    window.addEventListener("resize", updateDimensions);
    return () => window.removeEventListener("resize",updateDimensions);
  }, []);


  return <UserContext.Provider value={windowWidth}>
        <>
          <Head>
              <meta name="viewport" content="width=device-width, initial-scale=1" />
          </Head>
          <Component {...pageProps} />
        </>
  </UserContext.Provider>
}

export default MyApp
